package com.example.mygame.desktop

import com.example.mygame.Main
import playn.java.JavaPlatform
import playn.java.LWJGLPlatform

fun main(args: Array<String>) {
    val config = JavaPlatform.Config()
    config.width = 800
    config.height = 600
    // use config to customize the Java platform, if needed
    val plat = LWJGLPlatform(config)
    Main(plat)
    plat.start()
}
