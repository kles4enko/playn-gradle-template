plugins {
    application
    kotlin("jvm")
}

application {
    mainClassName = "com.example.mygame.desktop.MainDesktopKt"
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    val playnVersion:String by rootProject.extra

    implementation(project(":core"))
    implementation(project(":assets"))
    implementation("io.playn:playn-java-lwjgl:$playnVersion")
}