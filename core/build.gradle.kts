plugins {
    kotlin("jvm")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_7
    targetCompatibility = JavaVersion.VERSION_1_7
}

dependencies {
    val kotlinVersion:String by rootProject.extra
    val playnVersion:String by rootProject.extra
    val triplePlayVersion:String by rootProject.extra
    val junitVersion:String by rootProject.extra

    implementation(kotlin("stdlib", kotlinVersion))
    implementation("io.playn:playn-scene:$playnVersion")
    implementation("com.threerings:tripleplay:$triplePlayVersion")
    testImplementation("junit:junit:$junitVersion")
}