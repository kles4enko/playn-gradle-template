package com.example.mygame

import playn.core.Platform
import playn.scene.ImageLayer
import playn.scene.SceneGame

class Main(plat: Platform) : SceneGame(plat, 33) { // update our "simulation" 33ms (30 times per second)

    init {
        // create and add background image layer
        val bgImage = plat.assets().getImage("images/bg.png")
        val bgLayer = ImageLayer(bgImage)
        // scale the background to fill the screen
        bgLayer.setSize(plat.graphics().viewSize)
        rootLayer.add(bgLayer)
    }

}
