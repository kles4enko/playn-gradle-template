plugins {
    id("com.android.application")
    kotlin("android")
}

android {
    val minSdkVersion:Int by rootProject.extra
    val targetSdkVersion:Int by rootProject.extra
    val compileSdkVersion:Int by rootProject.extra
    val buildToolsVersion:String by rootProject.extra

    buildToolsVersion(buildToolsVersion)
    compileSdkVersion(compileSdkVersion)

    defaultConfig {
        minSdkVersion(minSdkVersion)
        targetSdkVersion(targetSdkVersion)

        applicationId = "com.example.mygame"
        versionCode = 1
        versionName = "1.0-SNAPSHOT"

    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        setSourceCompatibility(JavaVersion.VERSION_1_7)
        setTargetCompatibility(JavaVersion.VERSION_1_7)
    }
    sourceSets {
        getByName("androidTest").java.srcDirs("src/androidTest/kotlin")
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("test").java.srcDirs("src/test/kotlin")
    }
}

dependencies {
    val playnVersion:String by rootProject.extra

    implementation(project(":core"))
    implementation(project(":assets"))
    implementation("io.playn:playn-android:$playnVersion")
}