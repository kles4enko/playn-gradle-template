package com.example.mygame.android

import com.example.mygame.Main
import playn.android.GameActivity

class MainActivity : GameActivity() {

    override fun main() {
        Main(platform())
    }

}
