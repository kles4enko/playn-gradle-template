# Simple PlayN template project using gradle

### Description
It is a copy of [default PlayN's archetype project](https://github.com/playn/playn/tree/master/archetype) with using Gradle as a build system.

It's initially created for Android build for PlayN, because current archetype based on Maven which has a lot of missed features for modern android app.
Therefor, currently implemented Desktop and Android build only. 

### Notes
* Implementations and Gradle scripts have written in Kotlin.
* This branch uses Android Gradle Plugin 2.3.3 because Intellij IDEA for now supports it as max version.
* If you use Android Studio 3.0 and what to used Java 8 most of all Java 8 features, please, see android_studio_3 branch