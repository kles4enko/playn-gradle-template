import org.gradle.kotlin.dsl.extra

plugins {
    base
    kotlin("jvm") version "1.2.40" apply false
    kotlin("android") version "1.2.40" apply false
    id("com.android.application") version "3.1.1" apply false
}

// Versions conf
val kotlinVersion: String by extra { "1.2.40" }
val playnVersion: String by extra { "2.0.1" }
val triplePlayVersion: String by extra { "2.0.1" }
val junitVersion: String by extra { "4.12" }
val minSdkVersion:Int by extra { 19 }
val targetSdkVersion:Int by extra { 27 }
val compileSdkVersion:Int by extra { 27 }
val buildToolsVersion:String by extra { "27.0.3" }

allprojects {
    group = "com.example.mygame"
    version = "1.0-SNAPSHOT"

    repositories {
        mavenLocal()
        jcenter()
        google()
    }
}